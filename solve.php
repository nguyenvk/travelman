<?php
//Read file
$handle = fopen("cities.txt", "r");
$citiesData = [];
$cityIdArr = [];
$cityNameArr = [];
if ($handle) {
    $i = 0;
    while (($line = fgets($handle)) !== false) {
        $cityData = [];
        $line = preg_replace('!\s+!', ' ', $line);
        $line = trim($line);
        $lineArr = explode(" ", $line);
        $cityData['lon'] = (float) end($lineArr);
        array_pop($lineArr);
        $cityData['lat'] = (float) end($lineArr);
        array_pop($lineArr);
        $cityData['name'] = implode(" ", $lineArr);
        $cityData['id'] = $i;
        $cityNameArr[$i] = $cityData['name'];
        $citiesData[] = $cityData;
        $cityIdArr[] = $i;
        $i++;
    }
    fclose($handle);
} else {
    die("File not found");
}

//Function to caculate distance of 2 cities, copy from internet with some modification
function calDistance($lat1, $lon1, $lat2, $lon2) {
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    return rad2deg(acos($dist));
}

//Store distance of cities, array is odd, just foreach by half;
$distanceArr = [];
foreach ($citiesData as $cityData1) {
    foreach ($citiesData as $cityData2) {
        if (!isset($distanceArr[$cityData1['id']][$cityData2['id']])) {
            $distanceArr[$cityData1['id']][$cityData2['id']] = calDistance($cityData1['lat'], $cityData1['lon'], $cityData2['lat'], $cityData2['lon']);
            $distanceArr[$cityData2['id']][$cityData1['id']] = $distanceArr[$cityData1['id']][$cityData2['id']];
        }

    }
}
/**
 * First, I tried to just simple find all possible solution to travel all of the cities, then compare distance, but it take
 * much much time for script to execute, then I just think another solution
 *
 * If I select a city on the list (A city for example), then, repeatedly selected the closest cities ( closest of city A is B
 * , then closest of city B is C, then closest of city C is D .....) until all cities selected. Is it will be the shortest path
 * to travel from city that I picked, to the rest cities, one time each ? Then I selected 32 cities in turn, find the shortest path
 * when start from them, and compare the last result to find the real shortest path
 *
 * Since the distance of cities is Double, so It almost never have a pair of cities had the same distance.
 *
 * I'm not sure the theory above is correct or not, but it optimize to make the script run for lest than one second on my local machine :)
 */
$cityIdArrCopy = $cityIdArr;
//Starting point is most important, try all of them
$totalCity = count($cityIdArr);
$totalCitySub1 = $totalCity - 1;
//for ($i = 0; $i < $cityIdArr; $i++) {
$totalAllMinDistance = [];
$totalSolveCityWay = [];
foreach ($cityIdArr as $i) {
    // get the closest city that not came yet
    $trackingCityTravel = [];
    $trackingCityTravel[] = $i;
    $distance = 0;
    $cityNotComeYet = $cityIdArr;
    unset($cityNotComeYet[array_search($i, $cityNotComeYet)]);
    $totalDistance = 0;
    $cityPick = $i;
    for ($j = 0; $j < $totalCitySub1; $j++) {
        $min = 9999999999999999; //Very large number
        /**
         * loop vertical or horizontal just the same
         * Have to select current selected cities to get the correct distance
         */
        foreach ($distanceArr[$cityPick] as $toCityId => $distance) {
            if (in_array($toCityId, $cityNotComeYet)) {
                if ($distance < $min) {
                    $min = $distance;
                    $cityPick = $toCityId;
                }
            }
        }
        $totalDistance += $min;
        $trackingCityTravel[] = $cityPick;
        unset($cityNotComeYet[array_search($cityPick, $cityNotComeYet)]);
    }
    $totalSolveCityWay[] = $trackingCityTravel;
    $totalAllMinDistance[] = $totalDistance;

}

$keyMin = array_search(min($totalAllMinDistance), $totalAllMinDistance);
echo "Shortest route: \n";
foreach ($totalSolveCityWay[$keyMin] as $cId) {
    echo "    " . $cityNameArr[$cId] . "\n";
}
echo "\n";
echo "Total distance:" . $totalAllMinDistance[$keyMin] . " miles \n";
